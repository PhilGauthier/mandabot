# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Felix Bigand, Marie Soret, Philippine Prevost, Perrine Marsac @ UPMC

Updated on July 24 2019 to connect with robot, see isir package
@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

from Tkinter import *
import Tkinter
import time
import os
import pyaudio
import ttk

import boucleFermee
reload(boucleFermee)
import boucleOuverte
reload(boucleOuverte)
from isir import MandaBotFrame as mf

version = 1.0
import logging
from logging.config import fileConfig
fileConfig('log.ini')
logger = logging.getLogger()
logger.info("Mandabot starting, version: {}".format(version))

triggerOnSoundVar = None

top = Tkinter.Tk()
mandaBotFrame = None
selectedRecDeviceId = 1

def on_closing():
    global top, mandaBotFrame
    if mandaBotFrame.mandaBot:
        mandaBotFrame.mandaBot.stop()
        if mandaBotFrame:
            mandaBotFrame.destroy()
    if top:
        top.destroy()

top.protocol("WM_DELETE_WINDOW", on_closing)

def initAudioInputDevices():
    res = {}
    p = pyaudio.PyAudio()
    for i in range(p.get_device_count()):
        d = p.get_device_info_by_index(i)
        if d['maxInputChannels'] >0:
            key = d['index']
            name = d['name']
            res[name] = key
    return res

recDevicesMap = initAudioInputDevices()
    
def launchBF():
    pwd = os.getcwd()
    delay = float(E1.get())
    time_sim = float(E4.get())
    threshold = float(E5.get())
    ID = E2.get()
    if ID == '' :
        ID = 'Unknown_ID'
    timestamp = '%d' %(time.time())
    optFile = varFile.get()
    optAmp = varAmp.get()
    birdCode = ID+'_'+timestamp
    os.mkdir(pwd+'/Boucle_Fermee/'+birdCode)
    boucleFermee.boucleFermee(time_sim, threshold, optFile, optAmp, delay, birdCode, selectedRecDeviceId, mandaBotFrame, triggerOnSoundVar)
    
def launchBO():
    pwd = os.getcwd()
    time_sim = float(E4.get())
    threshold = float(E5.get())
    freq_sound = float(E6.get())
    ID=E2.get()
    if ID == '':
        ID = 'Unknown_ID'
    timestamp = '%d' %(time.time())
    birdCode = ID+'_'+timestamp
    os.mkdir(pwd+'/Boucle_Ouverte/'+birdCode)
    boucleOuverte.boucleOuverte(freq_sound, time_sim, threshold, birdCode, selectedRecDeviceId, mandaBotFrame, triggerOnSoundVar)



paramFrame = Frame(top)
inputFrame = LabelFrame(paramFrame, text="Input")
BOFrame = LabelFrame(inputFrame, text="Open-Loop")
BFFrame = LabelFrame(inputFrame, text="Closed-Loop")


#type de son renvoyer listtext
resFrame = Frame(BFFrame)
L3 = Label(resFrame,text="Response Type : ")
varFile = IntVar()
R1 = Radiobutton(resFrame, text="Random files in the dataset", variable=varFile, value=1)
R1.select()
R2 = Radiobutton(resFrame, text="Imitate Call", variable=varFile, value=2)

#amplitude check button
varAmp = IntVar()
C1 = Checkbutton(BFFrame, text="Random sound amplitude", variable=varAmp,
                 onvalue=1, offvalue=0, height=5, width=20)

#delay label + winget
delayFrame=Frame(BFFrame)
L1 = Label(delayFrame, text="Response delay (s) : ")
E1 = Entry(delayFrame, bd=5)
E1.insert(END, '0.1')

#Recording device ID
recDeviceCBox = None
def recDeviceChanged(event):
    global selectedRecDeviceId,recDeviceCBox
    selectedRecDeviceId = recDevicesMap[recDeviceCBox.get()]
    logger.info("Selected Rec device : {}".format(selectedRecDeviceId))

recDeviceFrame = Frame(inputFrame)
recDeviceLabel = Label(recDeviceFrame, text="Recording device : ")
recDeviceCBox = ttk.Combobox(recDeviceFrame, width=50, state="readonly")
recDeviceCBox.bind("<<ComboboxSelected>>", recDeviceChanged)
recDeviceCBox['values'] = recDevicesMap.keys()
recDeviceCBox.current(0)
recDeviceChanged(None)
recDeviceLabel.pack(side=LEFT)
recDeviceCBox.pack(side=RIGHT)

# time simu
timeFrame = Frame(inputFrame)
L4 = Label(timeFrame, text="Time Simulation (s) : ")
E4 = Entry(timeFrame, bd=5)
E4.insert(END, '10')

# threshold
threshFrame=Frame(inputFrame)
L5 = Label(threshFrame, text="Threshold for detection : ")
E5 = Entry(threshFrame, bd=5)
E5.insert(END, '1e-3')

# frequency for BO
freqFrame=Frame(BOFrame)
L6 = Label(freqFrame, text="Interval between the played-calls (s): ")
E6 = Entry(freqFrame, bd=5)
E6.insert(END, '2.5')

outputFrame=LabelFrame(paramFrame, text="Output")

# Bird identifiaction
idFrame=Frame(outputFrame)
L2 = Label(idFrame, text="Bird ID")
E2 = Entry(idFrame, bd=5)


buttonBO = Button(top, text="Launch Open-Loop Interaction", command=launchBO)
buttonBF = Button(top, text="Launch Closed-Loop Interaction", command=launchBF)

#Phil start
logReplayWithLoopVar = BooleanVar()
checkboxMandabotLog = Checkbutton(top, text="Replay mandabot log with loop", variable=logReplayWithLoopVar)
triggerOnSoundVar = BooleanVar()
checkboxMandabotSoundTrigger = Checkbutton(top, text="Trigger scenario on sound", variable=triggerOnSoundVar)

# Mandabot URL
mandaBotFrame = mf.MandaBotFrame(paramFrame, logReplayWithLoopVar)
mandaBotFrame.pack()

E2.insert(END, "1125.0404")#existing ID
#Phil end


paramFrame.pack(expand=1)

recDeviceFrame.pack()
inputFrame.pack()


timeFrame.pack()
L4.pack(side=LEFT)
E4.pack(side=RIGHT)
threshFrame.pack()
L5.pack(side=LEFT)
E5.pack(side=RIGHT)
BFFrame.pack(side=LEFT)
resFrame.pack()
L3.pack(side=LEFT)
R1.pack()
R2.pack(side=LEFT)
C1.pack()
delayFrame.pack()
L1.pack(side=LEFT)
E1.pack(side=RIGHT)
BOFrame.pack(side=RIGHT,expand=1,fill=BOTH)
freqFrame.pack(expand=1)
L6.pack(side=LEFT)
E6.pack(side=RIGHT)

outputFrame.pack(expand=1,fill=BOTH)
idFrame.pack()
L2.pack(side=LEFT)
E2.pack(side=RIGHT)
buttonBO.pack()
buttonBF.pack()
checkboxMandabotLog.pack()
checkboxMandabotSoundTrigger.pack()

logger.info("Mandabot ready")
top.winfo_toplevel().title("Mandabot - v{}".format(version))

top.mainloop()

logger.info("Mandabot closing")
