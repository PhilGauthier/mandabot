#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 15:04:18 2018

@author: Felix Bigand, Marie Soret, Philippine Prevost, Perrine Marsac @ UPMC

Updated on July 24 2019 to connect with robot, see isir package
@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

import pyaudio
import wave
import os
from numpy import sum, array, fromstring, int16,float32, linspace
import numpy as np
import gzip
import cPickle as pickle
import sys
from struct import unpack, pack
from utils import file_in_subfold
import time
from MicroStream import MicroStream
import scipy.io.wavfile as wav
import logging

logger = None
mandaBot = None
mandaBotFrame = None
triggerOnSoundVar = None

# Definition fontions
def playFromWave(filename,audio,CHUNK,gain=1):
    if os.path.isfile(filename):
        logger.info("Playing: {}".format(filename))
        if mandaBot:
            if triggerOnSoundVar and triggerOnSoundVar.get():
                mandaBot.trigger("sound")
            else:
                mandaBot.audioPlayIfAvailable(filename, int(100.0*gain))
        else:
            wf = wave.open(filename, 'rb')
            # open stream
            streamOut = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
                            channels=wf.getnchannels(),
                            rate=wf.getframerate(),
                            output=True)
            # read data
            data = wf.readframes(CHUNK)
            # play stream
            while len(data) > 0:
                s=np.fromstring(data, np.int16) * gain  # adapt amplitude
                data = pack('h'*len(s), *s)
                streamOut.write(data)
                data = wf.readframes(CHUNK)
            # stop stream
            streamOut.stop_stream()
            streamOut.close()
    else:
        logger.error("File not found: {}".format(filename))

def playFromData(data,audio,CHUNK,FORMAT=pyaudio.paFloat32):
    streamOut = audio.open(format=FORMAT, channels=1,
                    rate=44100, output=True,
                    frames_per_buffer=CHUNK)
    
    # play stream
    for i in range(0,CHUNK,len(data)-CHUNK): 
        buff=data[i,i+CHUNK]
        streamOut.write(buff)
    # stop stream
    streamOut.stop_stream()
    streamOut.close()
  
# Fonction principale    
def boucleFermee( time_sim = 10, threshold = 1e-3, optFile = 1, optAmp = 0, delay = 0.1, birdCode = 'Untitled', selectedRecDeviceId=0, mandabotFrame=None, triggerOnSound=None):
    global mandaBot, mandaBotFrame, logger, triggerOnSoundVar
    triggerOnSoundVar = triggerOnSound
    logger = logging.getLogger(__name__)

    logger.info('Boucle fermee')
        
    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    CHUNK = 1024
    SOUND_SECONDS = 0.22
    periodsize = (int)(2.0)**5
    nameCalls = ['1296_calls', '1440_calls', '1511_calls', '1513_calls']    
    fileCalls = []
    fileCalls = file_in_subfold('data/calls', fileCalls, '.wav')
    pwd = os.getcwd()

    # Mandabot feature
    mandaBotFrame = mandabotFrame
    mandaBot = mandabotFrame.mandaBot
    if mandaBot:
        if mandaBotFrame.logReplayWithLoopVar.get() and mandaBotFrame.logFileVar.get():
            mandaBot.startLogPlaying(mandaBotFrame.logFileVar.get())
            mandaBot.startRefresh()
        else:
            path = pwd+'/Boucle_Fermee/'+birdCode+'/'+birdCode+'-log.txt'
            mandaBotFrame.logFileVar.set(path)
            mandaBot.generateRandomUniformSequence(time_sim)
            mandaBot.startLogRecording(path)
    
    txt = open(pwd+'/Boucle_Fermee/'+birdCode+'/'+birdCode+'.txt', "w")
    txt.write('Bird Code : '+birdCode+'\nTime simulation : %f s \n' %time_sim)
    txt.write('Threshold : %f \n' %threshold)
    txt.write('Play mode (1:Files of the dataset, 2:Imitate call) : %d \n' %optFile)
    txt.write('Random amplitude (1:Yes, 0:No) : %d \n' %optAmp)
    txt.write('Response delay : %f s \n \n' %delay)
    txt.write('Mandabot triggerOnSoundVar: {}'.format(triggerOnSoundVar.get()))
    txt.write('Mandabot logReplay: {}, {}'.format(mandaBotFrame.logReplayWithLoopVar.get(), mandaBotFrame.logFileVar.get()))

    
    audio = pyaudio.PyAudio()
        
    # launch one call to imply call response
    WAVE_INPUT_FILENAME = "data/calls/CC1525_1.wav"
    #WAVE_INPUT_FILENAME = "Boucle_Fermee/1296_41756.382525_4_27_10_37_32_338_0-122.wav"
    playFromWave(WAVE_INPUT_FILENAME, audio, CHUNK)

    try:
        # start Recording
        streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True,
                        input_device_index=selectedRecDeviceId,
                        frames_per_buffer=CHUNK)
        streamMic._sampSize=int(audio.get_sample_size(FORMAT))
        stream = MicroStream(stream = streamMic, periodsize=periodsize)
        stream.start()

        logger.info("recording...")

        # Nombre d'echantillons
        SoundSize=int(RATE / periodsize * SOUND_SECONDS)
        birdDetect=0
        trashDetect=0

        tI=time.time()
        t=time.time()-tI
        while t < time_sim :
            # init
            buff2previous=[]
            sound=[]
            record=0
            listen=1

            while listen:
                t=time.time()-tI
                if t >= time_sim :
                    listen = 0
                    break
                data = stream.read()
                buff2previous.append(data)

                if len(buff2previous)>100:
                    del buff2previous[0]

                    E = sum(data ** 2)/periodsize
                    #print(E)
                    if record:
                        if len(sound)+len(buff2previous)<=SoundSize:
                            sound.append(data)
                        else :
                            soundwav=np.hstack(sound[0:-1])
                            soundwav_16b = np.int16(soundwav*(2**15))
                            logger.info('time : %.2f s' %t)
                            record = 0
                            listen = 0
                            break

                    else:
                        if E > threshold:
                            for i in range(0, len(buff2previous)-1):
                                sound.append(buff2previous[i])
                            record = 1

            if t >= time_sim:
                break
            # Extract feature
            call = []
            for i in range(0, len(sound)-1):
                data = sound[i]
                window = np.hamming(periodsize)
                res = np.log(abs(np.fft.rfft(window * data)) + 1e-50)
                call.append(res)

            call = np.vstack(call[0:-1])

            # Get the same length around the maximum
            min_shape=115
            indmx = np.where(call == call.max())[0][0]
            indbt = indmx
            indtp = indmx

            while True:
                if indbt > 0:
                    indbt -= 1

                if (indtp - indbt) == min_shape:
                    break

                if indtp < call.shape[0]:
                    indtp += 1

                if (indtp - indbt) == min_shape:
                    break

            call = call[indbt:indtp, :]

            def softmax(x):
                e_x = np.exp(x - np.max(x))
                return e_x / e_x.sum()

            # Network
            class Net(object):
                actNameToActFun = {'softmax': softmax}

                def __init__(self, W0, b0, act0, W1, b1, act1):
                    self.W0   = W0
                    self.b0   = b0
                    self.act0 = None##actNameToActFun[act0]
                    self.W1   = W1
                    self.b1   = b1
                    self.act1 = self.actNameToActFun[act1]

                def forward(self, input):
                    output = np.dot(input, W0) + b0
                    if self.act0 is not None:
                        output = self.act0(output)
                    output = self.act1(np.dot(output, W1) + b1)
                    return output

            name_net = '180437.pkl.gz'  # sys.argv[1]
            if not os.path.isfile(name_net):
                logger.error("Could not find file {}".format(name_net))
            else:
                f = gzip.open(name_net, 'r')
                net_params = pickle.load(f)
                f.close()

                W0 = net_params[0]['hidden_layer0']['W']
                b0 = net_params[0]['hidden_layer0']['b']
                act0 = None

                W1 = net_params[1]['output0']['W']
                b1 = net_params[1]['output0']['b']
                act1 = 'softmax'

                net = Net(W0, b0, act0, W1, b1, act1)

                # Reshape features for the network
                inputs = np.reshape(call, np.prod(call.shape))
                inputs = (inputs - inputs.min())/(inputs.max() - inputs.min())

                outputs = net.forward(inputs)
                if len(np.shape(outputs)) > 1:
                    binOut = np.argmax(outputs, axis=1)
                else:
                    binOut = np.argmax(outputs)

                logger.info("Sound detected (0 BIRD; 1 TRASH): {}".format(binOut))

                if binOut == 0:      #BIRD
                    birdDetect += 1
                    # Save in .wav
                    wav.write(pwd+"/Boucle_Fermee/"+birdCode+"/BIRD_"+birdCode+"_%d.wav" %birdDetect,RATE,soundwav_16b)
                    txt.write('time : %.4f s' %(time.time()-tI) +' \t'+"RECORDED : BIRD_"+birdCode+"_%d.wav \n" %birdDetect )
                    # stop Recording
                    streamMic.stop_stream()
                    streamMic.close()

                    time.sleep(delay)
                    if optAmp == 1:
                        gain = np.random.rand()*0.8 + 0.2 # random gain (de 0.2 à 1)
                    else:
                        gain = 1
                    if optFile == 1:
                        numCall = int(round(np.random.rand() * (len(fileCalls)-1)))
                        WAVE_INPUT_FILENAME = fileCalls[numCall]

                        playFromWave(WAVE_INPUT_FILENAME, audio, CHUNK, gain)
                        txt.write('time : %.4f s' %(time.time()-tI) +' \t'+"PLAYED : "+WAVE_INPUT_FILENAME +" \n" )
                    elif optFile == 2:
                        playFromWave(pwd+"/Boucle_Fermee/"+birdCode+"/BIRD_"+birdCode+"_%d.wav" %birdDetect,audio,CHUNK, gain)
                        txt.write('time : %.4f s' %(time.time()-tI) +' \t'+"PLAYED : BIRD_"+birdCode+"_%d.wav \n" %birdDetect )

                    streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                                rate=RATE, input=True,
                                frames_per_buffer=CHUNK)
                    streamMic._sampSize=int(audio.get_sample_size(FORMAT))

                    stream = MicroStream(stream = streamMic, periodsize=periodsize)
                    stream.start()
                else:      #TRASH
                    trashDetect += 1
                    # Save in .wav
                    wav.write(pwd+"/Boucle_Fermee/"+birdCode+"/TRASH_"+birdCode+"_%d.wav" % trashDetect, RATE, soundwav)
                    txt.write('time : %.4f s' %(time.time()-tI) + ' \t' + "RECORDED : TRASH_" + birdCode+"_%d.wav \n" % trashDetect )
                    # stop Recording
                    streamMic.stop_stream()
                    streamMic.close()

                    streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                                rate=RATE, input=True,
                                frames_per_buffer=CHUNK)
                    streamMic._sampSize = int(audio.get_sample_size(FORMAT))

                    stream = MicroStream(stream = streamMic, periodsize=periodsize)
                    stream.start()

        if mandaBot:
            mandaBot.stopLogRecording()

        # stop Recording
        streamMic.stop_stream()
        streamMic.close()
        audio.terminate()

        logger.info("end recording")
    except IOError as e:
        logger.error("Couldn't open microphone id: {}: {}".format(selectedRecDeviceId, e.message))
    
    #s_time = time.time()
    #for cnt in range(1000):
    #    output = net.forward(inputs)
    #print "done in (ms):", ((time.time()-s_time)/float(cnt) * 1000.0)
    
    #if binOut:
    #    #Affichage 2D
    #    import matplotlib.pyplot as plt
    #    ax = plt.axes()
    #    ax.imshow(call)
    #    ax.set_title('Recorded call TRASH');
    #    ax.set_ylabel('Time samples')
    #    ax.set_xlabel('Frequency samples')
    #else:
    #    #Affichage 2D
    #    import matplotlib.pyplot as plt
    #    ax = plt.axes()
    #    ax.imshow(call)
    #    ax.set_title('Recorded call BIRD');
    #    ax.set_ylabel('Time samples')
    #    ax.set_xlabel('Frequency samples')
     

