# Bird Project
## MandaBot
version 1.1 - Based on Nanterre Robot Bird Project. 

Updated on July 24 2019 to connect with a bird robot, most of the code is in **isir** package.
Update on January 31, to add beak control for the new robot

---
Authors: 
* ISIR UMR 7222, Software client : Philippe Gauthier - philippe.gauthier@sorbonne-universite.fr
* ISIR UMR 7222, harware & software server : Florian Richer - florian.richer@sorbonne-universite.fr
---
To run the program, the command is: **python main.py** *(using python v2.7 and modules in requirements.txt)*

If requirements are missing run: pip install -r requirements.txt

It control a bird robot while recording sounds. It can :
* rotate its body
* rotate its head
* wiggle its tail
* sing, while moving its beak, a preloaded wav files (loaded in sftp://debian:temppwd@192.168.8.1/audio)

It has different triggers above several thresholds, sensors are evaluated every 0.2 second :
* left perch
* right perch
* choc on the body (by mesuring acceleration)
* randomly
* sounds, using the base Bird project loop features

Sounds can trigger either (checkbox is the UI) :
* a wav file playback as response on the robot
* a random scenario file is played.

The **scenarii** folder contains for each type of trigger all the scenarii randomly chosed from.

Each scenario file is in json describing the sequence of movements or sings the robot achieve.
They can be played individually for testing in the UI.
```json
[
{"param":"tail","n":10, "period_ms":200,"delay":0.1}
,{"param":"audio","key":"callLive","delay":0.1}
,{"param":"head","move":5,"delay":0.5}
,{"param":"body","move":-15,"delay":0.5}
,{"param":"head","move":25,"delay":0.1}
,{"param":"body","move":0,"delay":0.1}
]
```
The log files of the robots is also in json, describing what scenario was triggered and when, they can be replayed with or without the original sound control loop.
```json
[
{"_time": 0.0, "_type": "init", "body": 5.27344, "chocThreshold": 0.05, "head": -25, "perchThreshold": 0.1}
,{"_time": 0.0, "_type": "action", "playAudio": "CC1525_1", "volume": 100}
,{"_time": 0.40, "_type": "action", "playScenario": "scenarii/choc/choc2.json"}
,{"_time": 1.37, "_type": "action", "playAudio": "CC1525_18", "volume": 78}
,{"_time": 4.59, "_type": "action", "playAudio": "CC1525_2", "volume": 62}
,{"_time": 6.40, "_type": "action", "playScenario": "scenarii/random/random1.json"}
,{"_time": 10.8, "_type": "action", "playAudio": "CC1525_22", "volume": 71}
,{"_time": 11.4, "_type": "action", "playAudio": "CC1525_4", "volume": 41}
,{"_time": 13.8, "_type": "action", "playScenario": "scenarii/right_perch/rightPerch1.json"}
,{"_time": 13.6, "_type": "action", "playAudio": "CC1525_2", "volume": 89}]
```
#### In open loop, the robot either :
* play regulary sounds through the robot 
* play regulary random "sound" scenario.
* replay a log file 

#### In close loop, the robot either :
* react to :
   * sounds
   * robots sensors (choc, perchs)
* replay a log file

In both open and close loops, when not replaying a log file, the "random" threshold can be used to add random behaviors to the robot.

## Nanterre Robot Bird Project
*Created on Wed Jan 24 15:21:44 2018*

*@author: Felix Bigand, Marie Soret, Philippine Prevost, Perrine Marsac @ UPMC*

The file to start understand the way the features are created is called createBirdDataset


The way in which I create the feature is mainly in the file llaf.py, in particular the following lines:
```python
 self.window = np.hamming(self.windowsize)
    def do(self, data):
        feature = super(LLAFSimpleBank,self).do(data)
        if feature is not None:
            return np.log( abs( np.fft.rfft(self.window * data) ) + 1e-50 )
```           

            
In order to create a stream of data from the microphone you should use pyaudio.
Here is an example of how to do it.
```python
import pyaudio

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "output.wav"

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

print("* recording")

frames = []

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)
    frames.append(data)

print("* done recording")

stream.stop_stream()
stream.close()
p.terminate()
```

It would be great if you could manage to create a class with the same functionality of WaveStream class 
(see the file wavestream.py) that takes as input instead of a wav file the stream you create from the microphone.
In case this seem too complex you can decice to create your code for evaluating the features from the audio stream.



