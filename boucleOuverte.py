#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 18:11:28 2018

@author: Felix Bigand, Marie Soret, Philippine Prevost, Perrine Marsac @ UPMC

Updated on July 24 2019 to connect with robot, see isir package
@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

import pyaudio
import wave
import os
from numpy import sum, array, fromstring, int16,float32, linspace
import numpy as np
import gzip
import cPickle as pickle
from utils import file_in_subfold
import time
from MicroStream import MicroStream
import scipy.io.wavfile as wav
from threading import *

import logging
logger = None

mandaBot = None
mandaBotFrame = None
triggerOnSoundVar = None
START = 1
STOP_MIC=0
txt_str=''

# Play audio file to out device
def playFromWave(filename, audio, CHUNK):
    if os.path.isfile(filename):
        logger.info("Playing {}".format(filename))
        if mandaBot:
            if triggerOnSoundVar and triggerOnSoundVar.get():
                mandaBot.trigger("sound")
            else:
                mandaBot.audioPlayIfAvailable(filename)
        else:
            wf = wave.open(filename, 'rb')
            # open stream
            streamOut = audio.open(format=audio.get_format_from_width(wf.getsampwidth()),
                            channels=wf.getnchannels(),
                            rate=wf.getframerate(),
                            output=True)

            # read data
            data = wf.readframes(CHUNK)
            # play stream
            while len(data) > 0:
                streamOut.write(data)
                data = wf.readframes(CHUNK)
            # stop stream
            streamOut.stop_stream()
            streamOut.close()
    else:
        logger.error("File not found: {}".format(filename))


def boucleOuverte(freq_sound=5, time_sim=10, threshold=1e-3, birdCode='Untitled', selectedRecDeviceId=0, mandabotFrame=None, triggerOnSound=None):
    global mandaBot, mandaBotFrame, logger, triggerOnSoundVar
    triggerOnSoundVar = triggerOnSound
    logger = logging.getLogger(__name__)

    logger.info('Boucle ouverte')

    FORMAT = pyaudio.paInt16
    CHANNELS = 2
    RATE = 44100
    CHUNK = 1024
    SOUND_SECONDS = 0.2
    periodsize = (int)(2.0)**5
    nameCalls = ['1296_calls', '1440_calls', '1511_calls', '1513_calls']    
    fileCalls = []
    fileCalls = file_in_subfold('data/calls', fileCalls, '.wav')
    
    pwd = os.getcwd()

    # Mandabot feature
    mandaBotFrame = mandabotFrame
    mandaBot = mandabotFrame.mandaBot
    if mandaBot:
        mandaBot.doTriggers = False
        if mandaBotFrame.logReplayWithLoopVar.get() and mandaBotFrame.logFileVar.get():
            mandaBot.startLogPlaying(mandaBotFrame.logFileVar.get())
            mandaBot.startRefresh()
        else:
            path = pwd+'/Boucle_Ouverte/'+birdCode+'/'+birdCode+'-log.txt'
            mandaBot.generateRandomUniformSequence(time_sim)
            mandaBot.startLogRecording(path)
            mandaBotFrame.logFileVar.set(path)

    txt = open(pwd+'/Boucle_Ouverte/'+birdCode+'/'+birdCode+'.txt',"w")
    txt.write('Bird Code : '+birdCode+'\nTime simulation : %f s \n' %time_sim)
    txt.write('Threshold : %f \n' %threshold)
    txt.write('Interval between the played-calls : %f s \n \n' %freq_sound)
    txt.write('Mandabot triggerOnSoundVar: {}'.format(triggerOnSoundVar.get()))
    txt.write('Mandabot logReplay: {}, {}'.format(mandaBotFrame.logReplayWithLoopVar.get(), mandaBotFrame.logFileVar.get()))

    global tI
    tI = time.time()

    audio = pyaudio.PyAudio()
    
    #########################################################################################
    #Classe des Threads
    class Son(Thread):

        def __init__(self,temps, duree):
            Thread.__init__(self)
            self.temps = temps
            self.duree = duree

        def run(self):
            global START
            global STOP_MIC
            global tI
            global txt_str
            while time.time()-tI < self.duree :
                #print (time.time()-tI) % (self.temps)
                if ( (time.time()-tI) % (self.temps) < 1e-2 ): #Permet de determiner le temps d'execution sinon on ne peut quitter qu'en fermant le programme totalement
                    logger.info('SonThreadStep')
                    numCall = int(round(np.random.rand()*(len(fileCalls)-1)))
                    WAVE_INPUT_FILENAME = fileCalls[numCall]
                    STOP_MIC=1
                    logger.info('time : %.4f s' %(time.time()-tI) +'  Sound played...')
                    txt_str=txt_str+'time : %.4f s' %(time.time()-tI) +' \t'+"PLAYED : "+WAVE_INPUT_FILENAME+"\n"
                    playFromWave(WAVE_INPUT_FILENAME, audio, CHUNK)
                    time.sleep(0.2)
                    STOP_MIC = 0
                #time.sleep(self.temps-0.2)
            START=0


    ######################################################################################

#    WAVE_INPUT_FILENAME = "callLive/1296_41756.382525_4_27_10_37_32_338_0-122.wav"
#    playFromWave(WAVE_INPUT_FILENAME,audio,CHUNK)

    class Enregistrement(Thread):
        def __init__(self):
            Thread.__init__(self)

            # Nombre d'echantillons
            self.SoundSize=int(RATE / periodsize * SOUND_SECONDS)
            self.birdDetect=0
            self.trashDetect=0

        def run(self):
        # start Recording
            global START
            global STOP_MIC
            global tI
            global txt_str
            pwd = os.getcwd()
            txt = open(pwd+'/Boucle_Ouverte/'+birdCode+'/'+birdCode+'.txt', "a")
            try:
                streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                                rate=RATE, input=True,
                                input_device_index=selectedRecDeviceId,
                                frames_per_buffer=CHUNK)
                streamMic._sampSize=int(audio.get_sample_size(FORMAT))
                stream = MicroStream(stream = streamMic, periodsize=periodsize)
                stream.start()

                while(START==1):
                    buff2previous=[]
                    sound=[]
                    record=0
                    listen=1

                    while listen:
                        data = stream.read()
                        buff2previous.append(data)

                        if len(buff2previous) > 100:
                            del buff2previous[0]

                            E=sum(data ** 2)/periodsize
                            #print(E)
                            if record:
                                if len(sound)+len(buff2previous) <= self.SoundSize:
                                    sound.append(data)
                                else :
                                    soundwav=np.hstack(sound[0:-1])
                                    soundwav_16b = np.int16(soundwav*(2**15))
                                    record=0
                                    listen=0
                                    break

                            else:
                                if E > threshold:
                                    for i in range(0, len(buff2previous)-1) :
                                        sound.append(buff2previous[i])
                                    record = 1
                    call = []
                    for i in range(0, len(sound)-1):
                        data = sound[i]
                        window = np.hamming(periodsize)
                        res = np.log( abs( np.fft.rfft(window * data) ) + 1e-50 )
                        call.append(res)

                    call = np.vstack(call[0:-1])

                    # Get the same length around the maximum
                    min_shape=115
                    indmx = np.where(call == call.max())[0][0]
                    indbt = indmx
                    indtp = indmx

                    while True:
                        if (indbt > 0):
                            indbt -= 1

                        if (indtp - indbt) == min_shape:
                            break

                        if (indtp < call.shape[0]):
                            indtp += 1

                        if (indtp - indbt) == min_shape:
                            break

                    call=call[indbt:indtp,:]

                    def softmax(x):
                        e_x = np.exp(x - np.max(x))
                        return e_x / e_x.sum()

                    # Network
                    class Net(object):
                        actNameToActFun = {'softmax' : softmax}

                        def __init__(self, W0, b0, act0, W1, b1, act1):
                            self.W0 = W0
                            self.b0 = b0
                            self.act0 = None##actNameToActFun[act0]
                            self.W1 = W1
                            self.b1 = b1
                            self.act1 = self.actNameToActFun[act1]

                        def forward(self, input):
                            output = np.dot(input, W0) + b0
                            if self.act0 is not None:
                                output = self.act0(output)

                            output = self.act1(np.dot(output, W1) + b1)
                            return output

                    name_net  = '180437.pkl.gz' #sys.argv[1]
                    f = gzip.open(name_net, 'r')
                    net_params = pickle.load(f)
                    f.close()

                    W0 = net_params[0]['hidden_layer0']['W']
                    b0 = net_params[0]['hidden_layer0']['b']
                    act0 = None

                    W1 = net_params[1]['output0']['W']
                    b1 = net_params[1]['output0']['b']
                    act1 = 'softmax'

                    net = Net(W0, b0, act0, W1, b1, act1)

                    # Reshape features for the network
                    inputs = np.reshape(call, np.prod(call.shape))
                    inputs = (inputs - inputs.min())/(inputs.max() - inputs.min())

                    outputs = net.forward(inputs)
                    if len(np.shape(outputs)) > 1:
                        binOut = np.argmax(outputs, axis=1)
                    else:
                        binOut = np.argmax(outputs)

                    if binOut == 0 and STOP_MIC == 0 and START == 1:      #BIRD
                        self.birdDetect += 1
                        logger.info('time : %.4f s' %(time.time()-tI)+'  Bird sound detected...')
                        # Save in .wav
                        wav.write(pwd+"/Boucle_Ouverte/"+birdCode+"/BIRD_"+birdCode+"_%d.wav" %self.birdDetect,RATE,soundwav_16b)
                        txt_str += 'time : %.4f s' %(time.time()-tI) +' \t'+"RECORDED : BIRD_"+birdCode+"_%d.wav \n" %self.birdDetect
                        # stop Recording
                        streamMic.stop_stream()
                        streamMic.close()

                        streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                               rate=RATE, input=True,
                               input_device_index=selectedRecDeviceId,
                               frames_per_buffer=CHUNK)
                        streamMic._sampSize=int(audio.get_sample_size(FORMAT))
                        stream = MicroStream(stream = streamMic, periodsize=periodsize)
                        stream.start()


                    elif ( (binOut==1) | (STOP_MIC==1) ) & (START==1) :      #TRASH
                        self.trashDetect+=1
                        logger.info('time : %.4f s' %(time.time()-tI) +'  Trash/Speaker sound detected...')
                        # Save in .wav
                        wav.write(pwd+"/Boucle_Ouverte/"+birdCode+"/TRASH_"+birdCode+"_%d.wav" %self.trashDetect,RATE,soundwav)
                        txt_str+='time : %.4f s' %(time.time()-tI) +' \t'+"RECORDED (includes loud-speaker) : TRASH_"+birdCode+"_%d.wav \n" %self.trashDetect
                        # stop Recording
                        streamMic.stop_stream()
                        streamMic.close()

                        streamMic = audio.open(format=FORMAT, channels=CHANNELS,
                                rate=RATE, input=True,
                                input_device_index=selectedRecDeviceId,
                                frames_per_buffer=CHUNK)
                        streamMic._sampSize=int(audio.get_sample_size(FORMAT))
                        stream = MicroStream(stream = streamMic, periodsize=periodsize)
                        stream.start()
            except IOError:
                logger.error("Couldn't open microphone id: {}".format(selectedRecDeviceId))

    thread_1 = Son(freq_sound, time_sim)
    thread_2 = Enregistrement()
    #thread_2=Afficheur("2")

    thread_1.start()
    thread_2.start()

    thread_1.join()
    thread_2.join()

    if mandaBot:
        mandaBot.doTriggers = True
        mandaBot.stopLogRecording()

    logger.info("Program stopped successfully !!! :D ")
    txt = open(pwd+'/Boucle_Ouverte/'+birdCode+'/'+birdCode+'.txt',"a")
    txt.write(txt_str)

