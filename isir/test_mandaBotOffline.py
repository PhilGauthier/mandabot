# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

from unittest import TestCase
from MandaBotController import *
import time
import json

class TestMandaBotController(TestCase):
    def setUp(self):
        self.m_dic = {}

        mandabotSnd = None
        self.mandaBot = MandaBotController('http://192.168.8.1:8081', "D:\dev\python\MandaBot",
                                      self.m_dic, mandabotSnd)

        self.mandaBot.setRandomUniformTimout(True, 1.)


    def tearDown(self):
        if self.mandaBot:
            self.mandaBot.stop()

    def test_generate_random_uniform_sequence(self):
        start = time.time()
        print("start: ",start)
        self.mandaBot.generateRandomUniformSequence(60)

        print(self.mandaBot.uniformRandomSequence)
        self.assertEquals(len(self.mandaBot.uniformRandomSequence), 60)

        self.mandaBot.setRandomUniformTimout(True, 2.)
        self.mandaBot.generateRandomUniformSequence(60)
        self.assertEquals(len(self.mandaBot.uniformRandomSequence), 30)

        self.mandaBot.setRandomUniformTimout(True, 3.)
        self.mandaBot.generateRandomUniformSequence(60)
        self.assertEquals(len(self.mandaBot.uniformRandomSequence), 20)

        self.mandaBot.setRandomUniformTimout(True, 2.)
        self.mandaBot.generateRandomUniformSequence(340)
        nb = len(self.mandaBot.uniformRandomSequence)
        print(nb)
        self.assertEquals(nb, 340/2)
        print("elapsed: ", time.time()-start)


