# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

from unittest import TestCase
from MandaBotController import *
import time
import json
import pyaudio
import wave

FORMAT = pyaudio.paInt16
CHANNELS = 2
RATE = 44100
CHUNK = 1024
RECORD_SECONDS = 5
WAVE_OUTPUT_FILENAME = "file.wav"

class TestPyAudio(TestCase):
    def setUp(self):
        self.audio = pyaudio.PyAudio()

    def tearDown(self):
        if self.audio:
            self.audio.terminate()

    def test_recording(self):
        stream = None
        id = 0
        device = None
        audio = self.audio
        while id < audio.get_device_count():
            try:
                device = audio.get_device_info_by_index(id)
                print("Trying id: {}, {}".format(id, device['name']))
                stream = audio.open(format=FORMAT, channels=CHANNELS,
                                    input_device_index=id,
                                    rate=RATE, input=True,
                                    frames_per_buffer=CHUNK)
                break
            except IOError:
                id += 1

        if stream:
            print "recording on device {}({})".format(device['name'], id)
            frames = []

            for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
                data = stream.read(CHUNK)
                frames.append(data)
            print "finished recording"

            # stop Recording
            stream.stop_stream()
            stream.close()

            waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
            waveFile.setnchannels(CHANNELS)
            waveFile.setsampwidth(audio.get_sample_size(FORMAT))
            waveFile.setframerate(RATE)
            waveFile.writeframes(b''.join(frames))
            waveFile.close()
        else:
            print("Couldn't start recording device")
