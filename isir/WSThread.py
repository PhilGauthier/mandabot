# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""
import time
import socket
from threading import Thread
import requests
import logging

logger = None


class WSThread(Thread):
    isRunning = False

    def __init__(self, base, datas, audioKeys):
        global logger
        logger = logging.getLogger(__name__)
        self.base = base
        self.datas = datas
        self.audioKeys = audioKeys

        self.nbReqFailed = 0
        self.nbReqFailedLimit = 5
        Thread.__init__(self)

    def run(self):
        if not WSThread.isRunning:
            WSThread.isRunning = True
            for i in range(0, len(self.datas)):
                param = self.datas[i]['param']
                delay = self.datas[i]['delay']

                url = None
                if param in ('body', 'head'):
                    url = "/{}/move?angle={}".format(param, -self.datas[i]['move'])
                elif param == 'tail':
                    url = "/{}/wiggle?n={}&period_ms={}".format(param, self.datas[i]['n'], self.datas[i]['period_ms'])
                elif param == 'audio':
                    key = self.datas[i]['key']
                    if key in self.audioKeys:
                        url = "/{}/play?n={}".format(param, self.audioKeys[key])
                    else:
                        logger.warn("Incorrect key: {}".format(key))

                if param and url and delay:
                    self.callUrlFor(self.base + url, delay)
                else:
                    logger.warn("Incorrect data read: {}".format(self.datas[i]))

            WSThread.isRunning = False

    def callUrlFor(self, url, delay):
        logger.debug("Playing {} for {} seconds".format(url, delay))
        try:
            r = requests.get(url, timeout=1)
            self.nbReqFailed =0
            content = r.text
            time.sleep(delay)
        except requests.exceptions.RequestException as e:
            self.nbReqFailed += 1
            if int(self.nbReqFailed / self.nbReqFailedLimit) > 0:
                logger.warn("Error: {} requests failed: {}".format(self.nbReqFailed, str(e)))
                self.nbReqFailedLimit *= 2
            return -1, e
        except socket.timeout as e:
            self.nbReqFailed += 1
            if int(self.nbReqFailed / self.nbReqFailedLimit) > 0:
                logger.warn("Error: {} requests failed: {}".format(self.nbReqFailed, str(e.reason)))
                self.nbReqFailedLimit *= 2
            return 524, e.reason
        return 200, content
