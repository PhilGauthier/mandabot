# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

# Use Tkinter for python 2, tkinter for python 3
import Tkinter as tk
import tkFileDialog
from Tkinter import *
import ttk
import os.path
import logging
import tkMessageBox

logger = None

from MandaBotController import *

def enableFrame(widget, isEnabled):
    if isinstance(widget, (Frame, LabelFrame)):
        for child in widget.winfo_children():
            enableFrame(child, isEnabled)
    else:
        try:
            if isEnabled:
                widget.configure(state='normal')
            else:
                widget.configure(state='disabled')
        except tk.TclError:
            pass


class MandaBotFrame(tk.Frame):
    def __del__(self):
        self.__stopUpdateUI()
        if self.mandaBot:
            self.mandaBot.stop()
        tk.Frame.__del__(self)

    def __init__(self, parent, logReplayWithLoopVar, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        global logger
        logger = logging.getLogger(__name__)
        self.parent = parent
        self._updateAfter = None
        self.mandaBot = None
        self.mandabotUrl = ""
        self.mandabotPath = os.getcwd()
        self.mandabotSnd = None

        self._entryUrl = None
        self._labelStatus = None

        self.mandabotTimerFreq = 150  # msec*
        self.connectedVar = BooleanVar()

        self.logReplayWithLoopVar = logReplayWithLoopVar
        self.statusVar = StringVar()
        self.statusVar.set('status')
        self.bodyVar = DoubleVar()
        self.headVar = DoubleVar()
        self.chocThresholdVar = DoubleVar()
        self.randomTriggerVar = DoubleVar()
        self.uniformRandomVar = BooleanVar()
        self.beakEnableVar = BooleanVar()
        self.uniformRandomTimoutVar = DoubleVar()
        self.perchLeftThresholdVar = DoubleVar()
        self.perchRightThresholdVar = DoubleVar()
        self.tailWiggleNVar = IntVar()
        self.tailWigglePeriodVar = IntVar()
        self.songVar = StringVar()
        self.scenarioVar = StringVar()
        self.logFileVar = StringVar()
        self.songStr = None
        self.audioVolumeVar = IntVar()
        self.audioVolumeVar.set(100)
        self.perchLeftThresholdVar.set(0.1)
        self.perchRightThresholdVar.set(0.03)

        self.tailWiggleNVar.set(3)
        self.tailWigglePeriodVar.set(200)
        self.uniformRandomTimoutVar.set(2)

        self.beakEnableVar.set(True)
        self.beakEnableVar.trace('w', self.beakEnable)
        self.connectedVar.trace('w', self.updateConnected)
        self.uniformRandomVar.trace('w', self.updateUniformRandom)
        self.uniformRandomTimoutVar.trace('w', self.updateUniformRandomTimeout)
        self.headVar.trace('w', self.updateHead)
        self.bodyVar.trace('w', self.updateBody)
        self.randomTriggerVar.trace('w', self.updateRandomTrigger)
        self.randomTriggerVar.set(0.000)
        self.chocThresholdVar.trace('w', self.updateChocThreshold)
        self.perchLeftThresholdVar.trace('w', self.updatePerchLeftThreshold)
        self.perchRightThresholdVar.trace('w', self.updatePerchRightThreshold)
        # Not used anymore
        #self.audioVolumeVar.trace('w', self.updateAudioVolume)

        self._imgFileAdd = PhotoImage(file="imgs/blue-document--plus.gif")
        self._imgFileEdit = PhotoImage(file="imgs/blue-document--pencil.gif")
        self._imgFolderSearch = PhotoImage(file="imgs/blue-folder-search-result.gif")
        self._imgFileOpen = PhotoImage(file="imgs/blue-folder-open-document-text.gif")
        self._imgOpen = PhotoImage(file="imgs/Document.gif")
        self._imgRec = PhotoImage(file="imgs/rec.gif")
        self._imgStop = PhotoImage(file="imgs/Symbol_Stop.gif")
        self._imgFolder = PhotoImage(file="imgs/Folder.gif")
        self._imgPlay = PhotoImage(file="imgs/Symbol_Play.gif")
        self._imgShutdown = PhotoImage(file="imgs/shutdown.gif")

        self._frame = LabelFrame(parent, text="MandaBot")

        self._innerLabelFrame = Frame(self._frame)
        Checkbutton(self._innerLabelFrame, text="Connect", variable=self.connectedVar).grid(row=0, column=0)
        self._labelUrl = Label(self._innerLabelFrame, text="URL : ")
        self._entryUrl = Entry(self._innerLabelFrame, bd=5, width=44)
        self._entryUrl.insert(END, 'http://192.168.8.1:8081')

        self._frameActions = Frame(self._frame)

        self._labelStatus = Entry(self._frame, textvariable=self.statusVar, state="readonly", justify='center')

        self._labelUrl.grid(row=0, column=1)
        self._entryUrl.grid(row=0, column=2)
        # Calibration
        self._buttonCalibration = Button(self._innerLabelFrame, text="Calibration", command=self.calibration)
        self._buttonCalibration.grid(row=0, column=3)

        # Shutdown
        self._buttonShutdown = Button(self._innerLabelFrame, text="Shutdown", image=self._imgShutdown, command=self.shutdown)
        self._buttonShutdown.grid(row=0, column=4)

        self._innerLabelFrame.pack()

        # Movement row
        # Body
        Label(self._frameActions, text="Body")\
            .grid(row=0, column=1)
        Spinbox(self._frameActions, textvariable=self.bodyVar, width=5, from_=-1, to=1., format="%.2f", increment=0.1)\
            .grid(row=0, column=2)
        # Head
        Label(self._frameActions, text="Head")\
            .grid(row=0, column=3)
        Spinbox(self._frameActions, textvariable=self.headVar, width=5, from_=-1, to=1., format="%.2f", increment=0.2)\
            .grid(row=0, column=4)

        # Tail
        self._frameTail = LabelFrame(self._frameActions, text="Tail")

        Label(self._frameTail, text="n") \
            .grid(row=0, column=0)
        Spinbox(self._frameTail, textvariable=self.tailWiggleNVar, width=5, from_=1, to=20, increment=1) \
            .grid(row=0, column=1)
        Label(self._frameTail, text="p") \
            .grid(row=0, column=2)
        Spinbox(self._frameTail, textvariable=self.tailWigglePeriodVar, width=5, from_=100, to=1000, increment=10) \
            .grid(row=0, column=3)
        Button(self._frameTail, text="Wiggle", command=self.tailWiggle) \
            .grid(row=0, column=4)
        self._frameTail.grid(row=0, column=5)

        # Thresholdq
        # Choc Threshold
        self._frameThresholds = LabelFrame(self._frameActions, text="Thresholds")
        Label(self._frameThresholds, text="Choc") \
            .grid(row=0, column=0)
        Spinbox(self._frameThresholds, textvariable=self.chocThresholdVar, width=5, from_=0.02, to=3.0, format="%.2f",
                increment=0.01) \
            .grid(row=0, column=1)
        # Perch Threshold
        Label(self._frameThresholds, text="Perch left") \
            .grid(row=0, column=2)
        Spinbox(self._frameThresholds, textvariable=self.perchLeftThresholdVar, width=5, from_=0.01, to=9.0, format="%.2f",
                increment=0.01) \
            .grid(row=0, column=3)
        Label(self._frameThresholds, text="Perch right") \
            .grid(row=0, column=4)
        Spinbox(self._frameThresholds, textvariable=self.perchRightThresholdVar, width=5, from_=0.01, to=9.0, format="%.2f",
                increment=0.01) \
            .grid(row=0, column=5)
        # Random Trigger
        Label(self._frameThresholds, text="Random") \
            .grid(row=0, column=6)
        Spinbox(self._frameThresholds, textvariable=self.randomTriggerVar, width=5, from_=0.00, to=1.0, format="%.3f",
                increment=0.001) \
            .grid(row=0, column=7)
        Checkbutton(self._frameThresholds, text="Uniform", variable=self.uniformRandomVar)\
            .grid(row=0, column=8)
        Spinbox(self._frameThresholds, textvariable=self.uniformRandomTimoutVar, width=5, from_=0.0, to=20.0, format="%.1f",
                increment=1.0) \
            .grid(row=0, column=9)
        self._frameThresholds.grid(row=0, column=10)

        self._frameActions.pack()

        self._beakAudioFrame = Frame(self._frame)
        # Beak
        self._frameBeakActions = LabelFrame(self._beakAudioFrame, text="Beak")
        Checkbutton(self._frameBeakActions, text="Enable", variable=self.beakEnableVar).grid(row=0, column=0)
        self._frameBeakActions.grid(row=0, column=1)


        # Audio row
        self._frameAudioActions = LabelFrame(self._beakAudioFrame, text="Audio")
        Button(self._frameAudioActions, text="Scan", command=self.audioScan) \
            .grid(row=0, column=0)
        self._audioCBox = ttk.Combobox(self._frameAudioActions, width=48, textvariable=self.songVar, state="readonly")
        self._audioCBox.bind("<<ComboboxSelected>>", self.audioCBoxSelection)
        self._audioCBox['values'] = ()
        self._audioCBox.grid(row=0, column=1)
        # Play audio
        Button(self._frameAudioActions, text="Play", command=self.audioPlay) \
            .grid(row=0, column=2)
        # Volume
        Label(self._frameAudioActions, text="Volume") \
            .grid(row=0, column=3)
        Spinbox(self._frameAudioActions, textvariable=self.audioVolumeVar, width=5, from_=0, to=100, increment=10) \
            .grid(row=0, column=4)
        self._frameAudioActions.grid(row=0, column=2)

        self._beakAudioFrame.pack()
        self._labelStatus.pack(fill=BOTH)

        imgSize = 16
        # Scenario row
        self._frameScenario = LabelFrame(self._frame, text="Scenario")
        Button(self._frameScenario, text="Open", command=self.openScenario, image=self._imgFolder, width=imgSize,
               height=imgSize) \
            .grid(row=0, column=1)
        Button(self._frameScenario, text="Folder", command=self.openScenariiFolder, image=self._imgFolderSearch, width=imgSize, height=imgSize) \
            .grid(row=0, column=0)
        self._scenarioField = Entry(self._frameScenario, textvariable=self.scenarioVar,\
                                    bd=5, width=29, state=DISABLED)\
            .grid(row=0, column=2)
        Button(self._frameScenario, text="Play", command=self.playScenario, image=self._imgPlay, width=imgSize, height=imgSize) \
            .grid(row=0, column=3)
        self._frameScenario.pack(side=tk.LEFT)

        # Log file row
        self._frameLog = LabelFrame(self._frame, text="Log file")
        Button(self._frameLog, text="Add", command=self.logFileAdd, image=self._imgFileAdd, width=imgSize,
               height=imgSize) \
            .grid(row=0, column=0)
        Button(self._frameLog, text="Edit", command=self.logFileEdit, image=self._imgFileEdit, width=imgSize, height=imgSize) \
            .grid(row=0, column=1)
        Button(self._frameLog, text="Browse", command=self.logFileBrowse, image=self._imgFolder, width=imgSize, height=imgSize) \
            .grid(row=0, column=2)
        self._logFileField = Entry(self._frameLog, textvariable=self.logFileVar, bd=5, width=25, state=DISABLED)\
            .grid(row=0, column=3)
        # play Button
        self._buttonPlay = Button(self._frameLog, command=self.logFilePlay, image=self._imgPlay, width=imgSize, height=imgSize)
        self._buttonPlay.grid(row=0, column=4)
        # Rec Button
        self._buttonRec = Button(self._frameLog, command=self.logFileRec, image=self._imgRec, width=imgSize, height=imgSize)
        self._buttonRec.grid(row=0, column=5)

        self._frameLog.pack(side=tk.RIGHT)

        self._frame.pack(expand=1, fill=BOTH)

        self.connect(False)

    def shutdown(self):
        if self.mandaBot:
            if tkMessageBox.askyesno("Shutdown robot",
                                   "Are you sure you want to shutdown the robot ?\n\nYou'll need to unplug and plug it again with the power cable."):
                self.mandaBot.shutdown()
                self.connectedVar.set(False)

    def logFilePlay(self):
        if self.mandaBot:
            file = self.logFileVar.get()
            if file and not self.mandaBot.isLogPlaying():
                self.mandaBot.startLogPlaying(file)
            elif self.mandaBot.isLogPlaying():
                self.mandaBot.stopLogPlaying()

    def logFileRec(self):
        if self.mandaBot:
            file = self.logFileVar.get()
            if file:
                if not self.mandaBot.isLogPlaying():
                    if self.mandaBot.isLogRecording():
                        self.mandaBot.stopLogRecording()
                    else:
                        self.mandaBot.startLogRecording(file)

    def openScenariiFolder(self):
        if self.mandaBot:
            self.mandaBot.openScenariiFolder()

    def logFileEdit(self):
        if self.mandaBot:
            filename = self.logFileVar.get()
            if filename:
                self.mandaBot.logFileEdit(filename)

    def logFileAdd(self):
        filename = self.logFileVar.get()
        initial_dir = None
        if filename:
            initial_dir = os.path.dirname(filename)
        file = tkFileDialog.asksaveasfile(parent=self.parent, \
                                        initialdir=initial_dir,
                                        mode='w', title='Choose a log file',
                                        filetypes=(("log files", "*-log.txt"), ("all files", "*.*")))
        if file:
            self.logFileVar.set(file.name)

    def logFileBrowse(self):
        filename = self.logFileVar.get()
        initial_dir = None
        if filename:
            initial_dir = os.path.dirname(filename)
        file = tkFileDialog.askopenfile(parent=self.parent, \
                                    initialdir=initial_dir,
                                    mode='r', title='Choose a log file',
                                    filetypes=(("log files", "*-log.txt"), ("all files", "*.*")))
        if file:
            self.logFileVar.set(file.name)

    def __startUpdateUI(self):
        self._updateAfter = self.after(self.mandabotTimerFreq, self.updateUI)

    def __stopUpdateUI(self):
        if self._updateAfter is not None:
            self.parent.after_cancel(self._updateAfter)
            self._updateAfter = None

    def playScenario(self):
        f = self.scenarioVar.get()
        if len(f) > 0 and os.path.isfile(f) and self.mandaBot:
            # print("Playing file: {}".format(f))
            self.mandaBot.playScenario(f)

    def openScenario(self):
        file = tkFileDialog.askopenfile(parent=self.parent, initialdir="{}/scenarii".format(self.mandabotPath), \
                    mode='rb', title='Choose a Scenario file', filetypes=(("json files", "*.json"), ("all files", "*.*")))
        if file != None:
            self.scenarioVar.set(file.name)

    def audioPlay(self):
        if self.mandaBot:
            self.mandaBot.audioPlay(self.songVar.get(), self.audioVolumeVar.get())

    def audioCBoxSelection(self, event):
        self.songStr = self.songVar.get()
        logger.info(self.songStr)

    def beakEnable(self, a, b, c):
        if c == 'w' and self.mandaBot:
            self.mandaBot.beakEnable(self.beakEnableVar.get())

    def updateConnected(self, a, b, c):
        if c == 'w':
            if self.mandaBot:
                self.connect(False)
            else:
                self.connect(True)

    def updateChocThreshold(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
                v = self.chocThresholdVar.get()
                self.mandaBot.setChocThreshold(v)
            except ValueError:
                logger.warn("Invalid Choc threshold volume")
                pass

    def updateRandomTrigger(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
                v = self.randomTriggerVar.get()
                self.mandaBot.randomTrigger = float(v)
            except ValueError:
                logger.warn("Invalid Choc threshold volume")
                pass

    def updateUniformRandom(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
               self.mandaBot.setRandomUniformTimout(self.uniformRandomVar.get(), self.uniformRandomTimoutVar.get())
            except ValueError:
                logger.warn("Invalid updateUniformRandom")
                pass

    def updateUniformRandomTimeout(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
                self.mandaBot.setRandomUniformTimout(self.uniformRandomVar.get(), self.uniformRandomTimoutVar.get())
            except ValueError:
                logger.warn("Invalid updateUniformRandom")
                pass

    def updatePerchLeftThreshold(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
                v = self.perchLeftThresholdVar.get()
                self.mandaBot.setPerchLeftThreshold(v)
            except ValueError:
                logger.warn("Invalid Choc threshold volume")
                pass

    def updatePerchRightThreshold(self, a, b, c):
        if c == 'w' and self.mandaBot:
            try:
                v = self.perchRightThresholdVar.get()
                self.mandaBot.setPerchRightThreshold(v)
            except ValueError:
                logger.warn("Invalid Choc threshold volume")
                pass

    # Not used anymore, volume is now specified each call
    # def updateAudioVolume(self, a,b,c):
    #     if c == 'w' and self.mandaBot:
    #         try:
    #             v = self.audioVolumeVar.get()
    #             self.mandaBot.setAudioVolume(v)
    #         except ValueError:
    #             logger.warn("Invalid audio volume")
    #             pass

    def tailWiggle(self):
        if self.mandaBot:
            n = self.tailWiggleNVar.get()
            p = self.tailWigglePeriodVar.get()
            self.mandaBot.tailWiggle(n, p)

    def updateHead(self, a,b,c):
        if self.mandaBot and c == 'w':
            v = self.headVar.get()
            self.mandaBot.moveHead(v)

    def updateBody(self, a, b, c):
        if self.mandaBot and c == 'w':
            v = self.bodyVar.get()
            self.mandaBot.moveBody(v)

    def audioScan(self):
        if self.mandaBot and self.mandaBot.audioScan():
                lst = self.mandaBot.audioList()
                self._audioCBox['values'] = lst.keys()
                self.mandaBot.status()

    def connect(self, doConnect):
        if doConnect:
            mandabotUrl = self._entryUrl.get()
            if not self.mandaBot:
                self.mandaBot = MandaBotController(mandabotUrl,
                                              self.mandabotPath, {},
                                              self.mandabotSnd)

                self.mandaBot.startRefresh()
            else:
                self.mandaBot.baseUrl = mandabotUrl
            self.mandaBot.chocReset()
            self.audioScan()
            self.mandaBot.sendInit(
                self.bodyVar.get(),
                self.headVar.get(),
                self.perchLeftThresholdVar.get(),
                self.perchRightThresholdVar.get(),
                self.chocThresholdVar.get(),
                self.randomTriggerVar.get(),
                self.uniformRandomVar.get(),
                self.uniformRandomTimoutVar.get()
            )
            self.__startUpdateUI()

            logger.info("Connected to {}".format(mandabotUrl))
        else:
            self.__stopUpdateUI()
            if self.mandaBot:
                self.mandaBot.chocReset()
                self.mandaBot.stop()
                logger.info("Disconnected from {}".format(self.mandaBot.baseUrl))
                self.mandaBot = None

        enableFrame(self._frameActions, doConnect)
        enableFrame(self._beakAudioFrame, doConnect)
        enableFrame(self._buttonCalibration, doConnect)
        enableFrame(self._buttonShutdown, doConnect)
        enableFrame(self._frameScenario, doConnect)
        enableFrame(self._frameLog, doConnect)

    def calibration(self):
        if self.mandaBot:
            logger.info("Calibration")
            self.mandaBot.calib()
        else:
            logger.warn("Please connect before calibrating")

    def updateUI(self):
        if not self.connectedVar.get():
            self.statusVar.set("")
        elif self.mandaBot:
            if self.mandaBot.isLogPlaying():
                self._buttonPlay.config(image=self._imgStop)
            else:
                self._buttonPlay.config(image=self._imgPlay)
            if self.mandaBot.isLogRecording():
                self._buttonRec.config(image=self._imgStop)
            else:
                self._buttonRec.config(image=self._imgRec)

            code, content = self.mandaBot.getLast()
            fg = "black"
            if code != 200:
                fg = "red"
            self._labelStatus.configure(fg=fg)
            self.statusVar.set(content)
            # print(code)

        self.__startUpdateUI()
