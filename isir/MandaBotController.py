import json
import collections
import MandaBotFile
import PeriodicTimer
import WSQueue
import WSThread
import re
import random
import os
import subprocess
import win32api
import logging
import time

logger = None

def getAudioName(filePath):
    matchObj = re.match(r'data/calls[\\/](.+).wav', filePath, re.M | re.I)
    if matchObj and matchObj.group():
        return matchObj.group(1)
    else:
        return None

class MandaBotController:
    """
    MandaBotController description
    ssh : debian:temppwd
    """

    def __init__(self, baseUrl, basePath, audioKeys, sndName):
        # The component to which this extension is attached
        global logger
        logger = logging.getLogger(__name__)
        self.baseUrl = baseUrl
        self.basePath = basePath
        self.audioKeys = audioKeys
        self.ws = WSQueue.WSQueue(.3)
        self.ws.start()
        self.status()
        self.body = 0.
        self.head = 0.
        self.logger = MandaBotFile.MandaBotFile()
        self.logContent = None
        self.refreshTimer = None
        self.refreshTimeout = 0.2
        self.perchTimeout = 4000
        self.perchLeftThreshold = 0.1
        self.perchRightThreshold = 0.1
        self.chocThreshold = 0.05
        self.chocCounter = 0
        self.chocCounterTimer = PeriodicTimer.PeriodicTimer(1, self.chocCounterTimout)
        self.chocCounterLimit = 10
        self.leftPerchUp = False
        self.leftPerchUpStart = None
        self.rightPerchUp = False
        self.rightPerchUpStart = None
        self.lastReadTime = None
        self.refreshTimer = PeriodicTimer.PeriodicTimer(self.refreshTimeout, self.update)
        self.doTriggers = True
        self.randomTrigger = 0.0
        self.uniformRandom = False
        self.uniformRandomTimout = 0.
        self.uniformRandomSequence = []
        self.uniformRandomStep = 0
        self.uniformRandomStepTimer = None
        self.uniformRandomDefaultSize = 60
        self.lastTriggerType = None

    def __del__(self):
        self.stop()

    def sendInit(self, body, head, perchLeftThreshold, perchRightThreshold, chocThreshold, randomTrigger, uniformRandom, uniformRandomTimout=0):
        logger.info("sendInit: body: {}, head: {}, perchLeftThr {}, perchRightThr {}, chocThr {}, randomTrigger {}, uniformRandom {}, uniformRandomTimout {}".
                    format(body, head, perchLeftThreshold, perchRightThreshold, chocThreshold, randomTrigger, uniformRandom, uniformRandomTimout))

        self.perchLeftThreshold = perchLeftThreshold
        self.perchRightThreshold = perchRightThreshold
        self.randomTrigger = randomTrigger
        self.uniformRandom = uniformRandom
        self.uniformRandomTimout = uniformRandomTimout
        self.__callUrl("/hbt?h={}&b={}&t={}".format(head, body, chocThreshold))

    def chocCounterTimout(self):
        if self.chocCounter > 0:
            self.chocCounter = self.chocCounter - 1

    def getScenariiDir(self, triggerName):
        return "{}/scenarii/{}".format(self.basePath, triggerName)

    def generateRandomUniformSequence(self, nbSeconds):
        nbSteps = 0
        if self.uniformRandom and self.uniformRandomTimout > 0.:
            nbSteps = int(nbSeconds / self.uniformRandomTimout)

            files = os.listdir(self.getScenariiDir("random"))
            self.uniformRandomSequence = []
            for i in range(0, nbSteps):
                self.uniformRandomSequence.append(files[i % len(files)])
            random.shuffle(self.uniformRandomSequence)
            self.uniformRandomStep = 0
            logger.info("Generated random seq: {}".format(self.uniformRandomSequence))
        else:
            logger.warn("Can't generate random uniform sequence for {}, {}seconds".format(self.uniformRandom, self.uniformRandomTimout ))

    def readInit(self, json):
        logger.info("readInit: {}".format(json))
        if 'body' in json:
            self.body = json['body']
        if 'head' in json:
            self.head = json['head']
        if 'perchLeftThreshold' in json:
            self.perchLeftThreshold = json['perchLeftThreshold']
        if 'perchRightThreshold' in json:
            self.perchRightThreshold = json['perchRightThreshold']
        if 'chocThreshold' in json:
            self.chocThreshold = json['chocThreshold']
        if 'randomTrigger' in json:
            self.randomTrigger = json['randomTrigger']
        if 'uniformRandom' in json:
            self.uniformRandom = json['uniformRandom']
        if 'uniformRandomTimout' in json:
            self.uniformRandomTimout = json['uniformRandomTimout']

    def getRndAudioKey(self):
        idx = random.randint(0, len(self.audioKeys))
        return self.audioKeys.keys()[idx]

    def openScenariiFolder(self):
        path = "{}\\scenarii\\".format(os.getcwd())
        logger.info("Opening {}".format(path))
        subprocess.Popen(r'explorer "'+path+'"')

    def logFileEdit(self, file):
        logger.info("Opening {}".format(file))
        win32api.ShellExecute(None, "edit", file, None, os.path.dirname(file), 1)

    def startRefresh(self):
        self.refreshTimer.start()
        self.chocCounterTimer.start()

    def stopRefresh(self):
        if self.refreshTimer:
            self.refreshTimer.cancel()
        if self.chocCounterTimer:
            self.chocCounterTimer.cancel()

    def startLogPlaying(self, filename):
        self.logger.play(filename)

    def startLogRecording(self, filename):
        self.logger.record(filename)
        self.logger.writeJson({'_type': 'init', 'body': self.body, 'head': self.head
                               , 'perchLeftThreshold': self.perchLeftThreshold, 'perchRightThreshold': self.perchRightThreshold
                               , 'chocThreshold': self.chocThreshold
                               , 'randomTrigger': self.randomTrigger
                               , 'uniformRandom': self.uniformRandom
                               , 'uniformRandomTimout': self.uniformRandomTimout})

    def getLogRecordingName(self):
        return self.logger.filename

    def stopLogRecording(self):
        self.logger.stop()

    def stopLogPlaying(self):
        self.logger.stop()
        #self.stopRefresh()

    def stop(self):
        self.ws.stop()
        self.stopRefresh()
        if self.logger:
            self.logger.stop()
        if self.uniformRandomStepTimer:
            self.uniformRandomStepTimer.cancel()

    def start(self):
        self.ws.start()
        self.startRefresh()

    def isLogPlaying(self):
        return self.logger.isLogPlaying()

    def isLogRecording(self):
        return self.logger.isLogRecording()

    def triggerLeftPerch(self, value):
        trigger = None
        threshold = self.perchLeftThreshold
        if not self.leftPerchUp and abs(value) > threshold:
            logger.info("left_perch {} above {}".format(value, threshold))
            self.leftPerchUp = True
            self.leftPerchUpStart = time.time()
            trigger = "on"
        elif self.leftPerchUp:
            if abs(value) > threshold and (time.time() - self.leftPerchUpStart > self.perchTimeout / 1000.):
                self.leftPerchUpStart = time.time()
                trigger = "on"
            elif abs(value) < threshold:
                self.leftPerchUp = False
                trigger = "off"
        return trigger

    def triggerRightPerch(self, value):
        trigger = None
        threshold = self.perchRightThreshold
        if not self.rightPerchUp and abs(value) > threshold:
            logger.info("right_perch {} above {}".format(value, threshold))
            self.rightPerchUp = True
            self.rightPerchUpStart = time.time()
            trigger = "on"
        elif self.rightPerchUp:
            elapsed = time.time() - self.rightPerchUpStart
            if abs(value) > threshold and elapsed > self.perchTimeout / 1000.:
                self.rightPerchUpStart = time.time()
                trigger = "on"
            elif abs(value) < threshold :
                self.rightPerchUp = False
                trigger = "off"
        return trigger

    def update(self):
        if self.isLogPlaying():

            d = self.logger.readJson()
            self.logContent = d

            if d and '_type' in d:
                if '_firstRead' in d and d['_firstRead']:
                    if d['_type'] == 'init':
                        uniform = False
                        uniformRandomTimout = 0.
                        if 'uniformRandom' in d:
                            uniform = d['uniformRandom']
                        if 'uniformRandomTimout' in d:
                            uniformRandomTimout = d['uniformRandomTimout']
                        self.sendInit(d['body'], d['head'], d['perchLeftThreshold'], d['perchRightThreshold'],
                                      d['chocThreshold'], d['randomTrigger'], uniform, uniformRandomTimout)
                    if d['_type'] == 'action':
                        if 'playScenario'in d:
                            self.playScenario(d['playScenario'])
                        if 'playAudio' in d:
                            if 'volume' in d and int(d['volume']) >= 0:
                                self.audioPlay(d['playAudio'], d['volume'])
                            else:
                                self.audioPlay(d['playAudio'])
                    logger.info("Replaying: {}".format(d))
                else:
                    if self.logger.EOF:
                        self.stopLogPlaying()
        else:
            hasTriggerred = False
            if self.doTriggers:
                self.status()
                res = self.ws.lastContent
                if res and len(res) > 0 and res[0] == '{':
                    jsonData = res  # "{{{}}}".format(t[1:-1])
                    d = json.loads(jsonData)
                    if 'body' in d:
                        self.body = d['body']
                    if 'head' in d:
                        self.head = d['head']
                    # For logging sensors info
                    #d['_type'] = 'read'; self.logger.writeJson(d)

                    if 'choc'in d and d['choc'] == 1:
                        self.chocCounter += 1
                        if self.chocCounter > self.chocCounterLimit:
                            self.trigger("chocDefense")
                        else:
                            self.trigger("choc")
                        self.chocReset()
                        hasTriggerred = True
                    if 'left_perch'in d:
                        onOff = self.triggerLeftPerch(d['left_perch'])
                        if onOff:
                            self.trigger("left_perch_"+onOff)
                            hasTriggerred = True
                    if 'right_perch'in d:
                        onOff = self.triggerRightPerch(d['right_perch'])
                        if onOff:
                            self.trigger("right_perch_"+onOff)
                            hasTriggerred = True
            if not self.uniformRandom:
                if not hasTriggerred and self.randomTrigger > random.uniform(0., 1.):
                    self.trigger("random")

    def playScenario(self, filename, t_type=None):
        if not WSThread.WSThread.isRunning or t_type == 'choc':
            file = None
            if os.path.isabs(filename):
                file = filename
            else:
                file = "{}/{}".format(self.basePath, filename)
            logger.info('Playing file: ' + filename)
            with open(file, 'r') as f:
                datas = json.load(f)
                if len(datas) > 0:
                    wsthread = WSThread.WSThread(self.baseUrl, datas,
                                        self.getAudioKeys())
                    wsthread.start()
                    self.logger.writeJson({'_type': 'action', 'playScenario': filename})

    def status(self):
        self.__callUrl('/status')

    def getNextUniformRandomName(self):
        if self.uniformRandomStep >= len(self.uniformRandomSequence):
            self.uniformRandomStep = 0
        if self.uniformRandomStep < len(self.uniformRandomSequence):
            filename = self.uniformRandomSequence[self.uniformRandomStep]
            self.uniformRandomStep += 1
            return filename
        return None

    def setRandomUniformTimout(self, enable, delay):
        logger.info("setRandomUniformTimout {} {}".format(str(enable), delay))
        self.uniformRandomTimout = float(delay)
        self.uniformRandom = bool(enable)
        if self.uniformRandomStepTimer:
            self.uniformRandomStepTimer.cancel()
        if self.uniformRandom and self.uniformRandomTimout > 0.:
            if len(self.uniformRandomSequence) == 0:
                self.generateRandomUniformSequence(self.uniformRandomDefaultSize)
            self.uniformRandomStepTimer = PeriodicTimer.PeriodicTimer(self.uniformRandomTimout,
                                                                      self.uniformRandomSetUpdate)
            self.uniformRandomStepTimer.start()

    def uniformRandomSetUpdate(self):
        self.trigger("random")

    def trigger(self, t_type):
        # Get a random file name in
        filename = None
        if 'random' == t_type and self.uniformRandom:
            filename = self.getNextUniformRandomName()
        else:
            filename = random.choice(os.listdir(self.getScenariiDir(t_type)))
        t_file = "scenarii/{}/{}".format(t_type, filename)
        #logger.info("Trigger detected, playing : {}".format(file))
        self.lastTriggerType = t_type
        self.playScenario(t_file, t_type)

    def chocReset(self):
        self.__callUrl('/adxl/reset')

    def audioScan(self):
        code, x = self.__callAndRes("/audio/scan", 0.1)
        return code == 200

    def cleanAudioKeys(self, audioKeys):
        newDict = {}
        for key in audioKeys.keys():
            oldKey = key
            matchObj = re.match(r'\/audio\/(.+).wav', key, re.M | re.I)
            if matchObj.group():
                key = matchObj.group(1)
            newDict[key] = audioKeys[oldKey]
        return newDict

    def audioList(self):
        code, res = self.__callAndRes("/audio/list", 0.1)
        self.audioKeys.clear()
        if code == 200:
            lines = res.splitlines()
            for line in lines:
                r = line.split(' ')
                key = r[1]
                value = int(r[0])
                self.audioKeys[key] = value
            self.audioKeys = collections.OrderedDict(sorted(self.audioKeys.items()))
            self.audioKeys = self.cleanAudioKeys(self.audioKeys)
        return self.audioKeys

    def getAudioKeys(self):
        if not(self.audioKeys and len(self.audioKeys)>0):
            self.audioList()
        return self.audioKeys

    def calib(self):
        #self.stopRefresh()
        self.__callUrl('/calib', 6)
        #self.startRefresh()

    def beakEnable(self, on):
        logger.warn("beakEnable: {}".format( on))
        if on:
            self.__callUrl('/beak/enable')
        else:
            self.__callUrl('/beak/disable')

    def shutdown(self):
        logger.warn("Shutting down robot")
        self.__callUrl('/shutdown', 6)

    def moveBody(self, angle):
        self.callUrl('/body/move?angle={}', int(-angle * 70.), )

    def moveHead(self, angle):
        self.callUrl('/head/move?angle={}', int(-angle * 60.))

    def tailWiggle(self, n, periodMs):
        #/tail/wiggle?n=[10]&period_ms=[100]
        self.__callUrl('/tail/wiggle?n={}&period_ms={}'.format(n, periodMs), (n*2)*periodMs/1000.)

    def audioPlayIfAvailable(self, filename, v=100):
        name = getAudioName(filename)
        if name:
            self.audioPlay(name, v)
        else:
            logger.warn("Audio file not on mandabot: {}".format(filename))

    def audioPlay(self, name, v=100):
        if len(self.audioKeys) > 0:
            if name in self.audioKeys:
                self.__audioPlay(self.audioKeys[name], v)
                self.logger.writeJson({'_type': 'action', 'playAudio': name, 'volume': v})
            else:
                logger.info("Audio key '{}' not found".format(name))
        else:
            logger.info("Audio dictionary is empty, scan for update")

    def __audioPlay(self, n, v=100):
        self.__callUrl('/audio/play?n={}&v={}'.format(int(n), int(v)))

    def setAudioVolume(self, n):
        self.callUrl('/audio/volume?n={}', int(n))

    def setChocThreshold(self, n):
        logger.info("setChocThreshold {}".format(n))
        self.chocThreshold = n
        self.callUrl('/adxl/threshold?n={}', float(n))

    def setPerchLeftThreshold(self, n):
        logger.info("setPerchLeftThreshold {}".format(n))
        self.perchLeftThreshold = n

    def setPerchRightThreshold(self, n):
        logger.info("setPerchRightThreshold {}".format(n))
        self.perchRightThreshold = n

    def callUrl(self, param, value):
        self.__callUrl(param.format(value))

    def getLast(self):
        if self.isLogPlaying():
            return 200, self.logContent
        return self.ws.lastCode, self.ws.lastContent

    def __callUrl(self, path, delay=None):
        url = self.baseUrl + path
        logger.debug(url)
        if not delay:
            self.ws.append(url)
        else:
            self.ws.callUrlFor(url, delay)

    def __callAndRes(self, path, delay):
        url = self.baseUrl + path
        return self.ws.callUrlFor(url, delay)
