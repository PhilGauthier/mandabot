# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

import time
import json
import os.path
import logging
import fasteners
logger = None

class Status:
    READY = 1
    PLAYING = 2
    RECORDING = 3

class MandaBotFile:
    def __init__(self, filename=None):
        # The component to which this extension is attached
        global logger
        logger = logging.getLogger(__name__)
        self.filename = filename
        self.status = Status.READY
        self.file = None
        self.startTime = 0
        self.currentJson = None
        self.nextJson = None
        self.EOF = False
        self.didFirstLine = False
        self.lock = fasteners.InterProcessLock("MandaBotFile.lock")

    def record(self, filename=None):
        if filename:
            self.filename = filename

        if self.status == Status.READY:
            self.file = open(self.filename, "w")
            self.startTime = time.time()
            self.status = Status.RECORDING
            self.file.write("[")
            self.didFirstLine = False
            logger.info("Recording on file: {}".format(self.filename))

    def play(self, filename=None):
        if self.status == Status.READY:
            if filename:
                self.filename = filename
            if os.path.isfile(self.filename):
                logger.info("Starting playing log {}".format(self.filename))
                self.file = open(self.filename, "r")
                self.startTime = time.time()
                self.status = Status.PLAYING
                self.EOF = False
            else:
                logger.warn("File doesn't exist: {}".format(self.filename))

    def __logReadLine(self):
        line = self.file.readline()
        if line:
            if line[0] == "[":
                line = self.file.readline()
            if line.find(',{') >= 0:
                line = line[1:]
            if line.rfind(']') > 0:
                line = line[:line.rindex(']')]
            try:
                d = json.loads(line)
                d['_firstRead'] = True
                return d
            except ValueError:
                logger.error("Coundn't read json: {}".format(line))

        return None

    def isLogPlaying(self):
        return self.status == Status.PLAYING

    def isLogRecording(self):
        return self.status == Status.RECORDING

    def readJson(self):
        if self.isLogPlaying():
            timeElasped = time.time() - self.startTime

            # Load current step and next if possible
            if self.currentJson:
                self.currentJson['_firstRead'] = False
            else:
                if not self.EOF:
                    self.currentJson = self.__logReadLine()

            if self.currentJson and not self.nextJson and not self.EOF:
                self.nextJson = self.__logReadLine()

            if not self.nextJson and not self.EOF:
                logger.info("EOF reached: {}".format(self.filename))
                self.EOF = True

            # should we load next step
            if self.nextJson and timeElasped >= self.nextJson['_time']:
                self.currentJson = self.nextJson
                self.nextJson = None

        return self.currentJson


    def writeJson(self, jsonMsg):
        if self.status == Status.RECORDING:
            timeElasped = time.time() - self.startTime
            jsonMsg['_time'] = timeElasped

            if self.didFirstLine:
                self.file.write("\n,")
            else:
                self.file.write("\n")
                self.didFirstLine = True
            msg = json.dumps(jsonMsg, sort_keys=True)
            if self.lock.acquire(timeout=1):
                logger.info(msg)
                self.file.write(msg)
                self.lock.release()
            else:
                logger.error("Coundn't acquire write lock : {}\n{}".format(self.filename,msg))


    def stop(self):
        if self.status in (Status.RECORDING, Status.PLAYING):
            if self.file:
                if self.status ==Status.RECORDING:
                    self.file.write("]")
                self.file.close()
                logger.info("Closing file: {}".format(self.filename))
            self.status = Status.READY
