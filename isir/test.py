from MandaBotController import *

mandaBot = None

def frange(start, stop, step):
    x = start
    sign = 1
    if step<0. and start > stop:
        sign = -1
    while sign* x < stop:
        yield x
        x += step

def printLeftTrigger(x):
    res = mandaBot.triggerLeftPerch(x)
    if res:
        print("x: ", x, res)

def printRightTrigger(x):
    res = mandaBot.triggerRightPerch(x)
    if res:
        print("x: ", x, res)

if __name__ == "__main__":
    # execute only if run as a script
    m_dic = {}

    mandabotSnd = None
    mandaBot = MandaBotController('http://192.168.8.1:8081', "D:/clouds/isir/ISIR/2019/MandaBot/src/MandaBot",
                                       m_dic, mandabotSnd)
    mandaBot.setPerchLeftThreshold(.5)
    mandaBot.setPerchRightThreshold(.5)
    print("left")
    for i in range(2):
        for x in frange(0., 1., 0.05):
            printLeftTrigger(x)
        for x in frange(1., 0., -0.05):
            printLeftTrigger(x)

    print("right")
    for i in range(2):
        for x in frange(0., 1., 0.05):
            printRightTrigger(x)
        for x in frange(1., 0., -0.05):
            printRightTrigger(x)