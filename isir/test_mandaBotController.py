# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

from unittest import TestCase
from MandaBotController import *
import time
import json

class TestMandaBotController(TestCase):
    def setUp(self):
        self.m_dic = {}

        mandabotSnd = None
        self.mandaBot = MandaBotController('http://192.168.8.1:8081', "D:\dev\python\MandaBot",
                                      self.m_dic, mandabotSnd)

    def tearDown(self):
        if self.mandaBot:
            self.mandaBot.stop()


    def test_connect_read_status(self):
        self.mandaBot.startRefresh()
        self.mandaBot.doTriggers = False
        i = 0
        while i < 5:
            i += 1
            time.sleep(1)

            code, res = self.mandaBot.getLast()
            self.assertEquals(code, 200)
            self.assertNotEqual(res, None)
            data = json.loads(res)
            self.assertNotEqual(data, None)
            print(data)

    def test_connect_log_record(self):
        self.mandaBot.startRefresh()
        self.mandaBot.doTriggers = True
        self.mandaBot.startLogRecording("testLog.json")
        i = 0
        while i < 3:
            i += 1
            time.sleep(1)

            code, res = self.mandaBot.getLast()
            self.assertEquals(code, 200)
            self.assertNotEqual(res, None)

            print(res)
        self.mandaBot.stopLogRecording()

    def test_connect_log_play(self):
        self.mandaBot.startRefresh()
        self.mandaBot.doTriggers = False
        self.mandaBot.startLogPlaying("testLog.json")
        i = 0
        while i < 3:
            i += 1
            time.sleep(1)

            code, res = self.mandaBot.getLast()
            #self.assertEquals(code, 200)
            #self.assertNotEqual(res, None)
            print(res)
        self.mandaBot.stopLogPlaying()
