# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 15:21:44 2018

@author: Philippe Gauthier @ ISIR Sorbonne-université
"""

import time
import Queue as queue
import socket
from threading import Thread
import atexit
import requests
import logging

logger = None

class WSQueue(Thread):
    def __init__(self, delay):
        global logger
        logger = logging.getLogger(__name__)
        self.delay = delay
        self.outQueue = queue.LifoQueue()
        self.inQueue = queue.Queue()
        self.isRunning = False
        self.isTimingOut = False
        self.lastCode = 0
        self.lastContent = None
        self.nbReqFailed = 0
        self.nbReqFailedLimit = 10
        atexit.register(self.stop)
        Thread.__init__(self)

    def stop(self):
        logger.info("Running cleanup...")
        with self.inQueue.mutex:
            self.isTimingOut = True
            self.inQueue.queue.clear()
        self.isRunning = False

    def append(self, data):
        self.inQueue.put(data)

    def stop(self):
        self.isRunning = False

    def get(self):
        res = None
        with self.outQueue.mutex:
            res = self.outQueue.get()
            self.outQueue.queue.clear()
        return res

    def run(self):
        self.isRunning = True
        while self.isRunning:
            while not self.inQueue.empty() and not (self.isTimingOut):
                url = self.inQueue.get()
                self.callUrlFor(url)
                self.outQueue.put(self.lastCode)
            self.isTimingOut = False
            time.sleep(.5)

    def callUrlFor(self, url, delay=None):

        logger.debug("call '{}'".format(url))
        content = None
        if not delay:
            delay = self.delay
        try:
            r = requests.get(url, timeout=delay)
#            r.raise_for_status()
            self.lastCode = r.status_code
            self.lastContent = r.text
            self.nbReqFailed = 0
        except requests.exceptions.RequestException as e:
            with self.inQueue.mutex:
                self.isTimingOut = True
                self.inQueue.queue.clear()
            self.lastCode = -1
            self.nbReqFailed += 1
            if int(self.nbReqFailed / self.nbReqFailedLimit) > 0:
                logger.warn("Error: {} requests failed: {}".format(self.nbReqFailed, str(e.message)))
                self.nbReqFailedLimit *= 2
        return self.lastCode, self.lastContent
