
from unittest import TestCase
from MandaBotController import *
import time

class TestMandaBotController(TestCase):
    def setUp(self):
        self.m_dic = {}

        mandabotSnd = None
        self.mandaBot = MandaBotController('http://192.168.8.1:8081', "D:\dev\python\MandaBot",
                                      self.m_dic, mandabotSnd)

        self.mandaBot.setRandomUniformTimout(True, 1.)


    def tearDown(self):
        if self.mandaBot:
            self.mandaBot.stop()

    def test_write_log_file(self):
        start = time.time()
        print("start: ", start)
        self.mandaBot.startRefresh()
        self.mandaBot.doTriggers = True
        self.mandaBot.generateRandomUniformSequence(60)
        self.mandaBot.startLogRecording('test-log.txt')
        self.mandaBot.trigger("choc"); time.sleep(.1)
        self.mandaBot.trigger("choc"); time.sleep(.1)
        self.mandaBot.trigger("choc"); time.sleep(.1)
        self.mandaBot.trigger("choc"); time.sleep(.1)
        self.mandaBot.trigger("choc"); time.sleep(.1)
        self.mandaBot.logger.writeJson({'_type': 'action', 'playAudio': "test", 'volume': 50});
        self.mandaBot.logger.writeJson({'_type': 'action', 'playAudio': "test", 'volume': 100}); time.sleep(.1)
        self.mandaBot.trigger("choc")
        self.mandaBot.stopLogRecording()