import numpy as np
from sstream import *
import wave

class MicroStream(SStream):
    ''' Stream for an microphone input
    '''
    def __init__(self, stream = None, rate = None, *args, **kwargs):
        super(MicroStream, self).__init__(*args, **kwargs)
        self.set_stream(stream, rate)
        
    def set_stream(self, stream, rate = None):
        self.st=stream;
        self.rate = self.st._rate
        self.channels = self.st._channels
        if self.st._sampSize != 2:
            print 'Only 16 bit files supported'
            raise IOError
        self._setflag_('StreamEnded', False)
        self._reset_()

    def set_position(self, pos):
        if self.st is not None:
            self.st.setpos(pos * 1e-3 * self.rate)
            self.position = int(pos * 1e-3 * self.rate)
            self._setflag_('StreamEnded', False)
            self._reset_()
        else:
            self._setflag_('StreamEnded', True, 'No wav specified')

    def get_position(self):
        if self.st is not None:
            return self.position * 1e3 / self.rate
        else:
            return None
            
    def start(self):
        super(MicroStream, self).start()
        if self.st is None:
            self._setflag_('StreamEnded', True, 'No wav specified')
        
    def _read_(self, n = None):
        if self.metadata['StreamEnded'] or self.st is None:
            return None
        if n is None:
            n = self.periodsize
        dat = self.st.read(n)
        if dat == '':
            self._setflag_( 'StreamEnded', True, 'Empty wav data; possibly EOF')
            return None
        dat = np.fromstring(dat, dtype=np.int16)[::self.channels].astype(np.float32) / 2**15
        if dat.shape[0] != n:
            self._setflag_( 'StreamEnded', True, 'Wrong wav data shape: %d, expected: %d; possibly EOF' % (dat.shape[0], n) )
            return None
        return dat
        
    def close(self):
        #self.wav.close()
        pass
